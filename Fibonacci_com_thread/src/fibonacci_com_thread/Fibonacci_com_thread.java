package fibonacci_com_thread;
import java.util.Scanner;
class Fibonacci_com_thread{
  private static Scanner input = new Scanner(System.in);
  public static void main(String[] args) throws InterruptedException {
    System.out.println("Escreva uma quantidade desejada para fibonacci:");
    ThreadFibo.end = input.nextLong();
    Thread t1 = new Thread(new ThreadFibo());
    Thread t2 = new Thread(new ThreadFibo());

    long start = System.currentTimeMillis(); 
    t2.start();
    t1.start();
    t1.join();
    t2.join();

    System.out.println("\n\n\n"+(-start + System.currentTimeMillis())+ " milisegundos");
  }
}