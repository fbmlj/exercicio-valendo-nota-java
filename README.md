
# **Trabalho de _Threads_ e _Swing_**

Essa pasta contém os seguintes exercícios:

---

## **Swing**

- ### Agenda de Contatos
  
  - A _main_ está em _InterfaceGrafica_

---

## **Com e Sem _Threads_**

- ### Fibonacci

- ### Somatório

---

## **Componentes do Grupo**

- ### Eduardo Canellas de Oliveira

- ### Guilherme Freitas França

- ### Lucas Tavares Sousa

- ### Pedro Trinkenreich Henrique

---
