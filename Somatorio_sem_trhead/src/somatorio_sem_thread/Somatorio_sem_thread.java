
package somatorio_sem_thread;

import java.util.Scanner;
class Somatorio_sem_thread {
  public static long limite;
  public static long somador;
  private static final Scanner input = new Scanner(System.in);
  
  public static void main(String[] args) {
      System.out.print("Escreva o limite do somador");
      limite  = input.nextLong();
     long i;
     somador = 0;
     long start = System.currentTimeMillis();
     for(i = 0;i <= limite;i++) somador += i; 
     System.out.println("Resultado = " +somador);
     System.out.println(System.currentTimeMillis() - start + " milisegundos");
  }
}