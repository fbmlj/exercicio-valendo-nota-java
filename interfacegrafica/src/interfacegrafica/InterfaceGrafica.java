/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacegrafica;
import agenda.ListaTelefonica;
import java.awt.BorderLayout;
import javax.swing.JFrame;
/**
 *
 * @author lucio
 */
public class InterfaceGrafica {

    /**
     * @param args the command line arguments
     */
    public static ListaTelefonica listatelefonica;
    public static ListaContatos listacontatos;
    public static Formulario formulario;
    public static void main(String[] args) {
        listatelefonica = new ListaTelefonica();
       
        listacontatos = new ListaContatos();
        formulario = new Formulario();
        JFrame janela = new JFrame("Minha janela");
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janela.getContentPane().add(listacontatos, BorderLayout.WEST);
        janela.getContentPane().add(formulario,  BorderLayout.EAST);
        
        janela.pack();
        janela.setVisible(true);
        
        
        
    }
    
}
