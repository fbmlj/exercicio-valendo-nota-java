package interfacegrafica;





import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.Dimension;
/**
 *
 * @author lucio
 */
public class Formulario extends JPanel implements ActionListener{
    private JLabel label_detalhes;
    private JTextArea input_detalhes = new JTextArea (5, 20);
    private JTextField input_telefone;
    private JLabel label_telefone;
    private JTextField input_nome;
    private JLabel label_nome;
    private JButton salvar_botao;
    private JButton excluir_botao;
    private JButton atualizar_botao;
    private boolean pode_criar = true;
    private String cur_name;
            
    public Formulario(){
        label_nome = new JLabel("nome:");
        input_nome = new JTextField (20);
        add(label_nome);
        add(input_nome);
        
        setPreferredSize(new Dimension(300,200));
        label_telefone = new JLabel("telefone:");
        input_telefone = new JTextField (20);
        add(label_telefone);
        add(input_telefone);
        
        label_detalhes= new JLabel("Detalhes:");
        input_detalhes = new JTextArea (5, 20); 
        add(label_detalhes);
        add(input_detalhes);
        
        salvar_botao = new JButton("Salvar");
        add(salvar_botao);
        salvar_botao.addActionListener(this);
        
        excluir_botao = new JButton("Excluir");
        add(excluir_botao);
        excluir_botao.addActionListener(this);
        
        atualizar_botao = new JButton("Atualizar");
        add(atualizar_botao,BorderLayout.SOUTH);
        atualizar_botao.addActionListener(this);
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (pode_criar){
            if (e.getSource() == salvar_botao){
                if (input_nome.getText().length()==0) return;

                InterfaceGrafica.listatelefonica.adicionar_contato(input_nome.getText(),input_telefone.getText() ,input_detalhes.getText() );
                InterfaceGrafica.listacontatos.atualizar_pessoas();
                remove_info();
            }
        }
        else{
           
            if (e.getSource() == atualizar_botao) {
                InterfaceGrafica.listatelefonica.editar_contato(cur_name, input_nome.getText(),input_telefone.getText() ,input_detalhes.getText() );
                remove_info();
            }
            
            if (e.getSource() == excluir_botao){
                InterfaceGrafica.listatelefonica.excluir_contato(cur_name);
                remove_info();
            }
                
            InterfaceGrafica.listacontatos.atualizar_pessoas();
        }
            
    }
    public void enable_buttons(){
        
        salvar_botao.setEnabled(pode_criar);
        atualizar_botao.setEnabled(!pode_criar);
        excluir_botao.setEnabled(!pode_criar);
    }
    
    public void set_info(String nome, String telefone, String detalhes){
        pode_criar = false;
        input_nome.setText(nome);
        input_detalhes.setText(detalhes);
        input_telefone.setText(telefone);
        cur_name = nome;
    }
    public void remove_info(){
        pode_criar = true;
        input_nome.setText("");
        input_detalhes.setText("");
        input_telefone.setText("");
    }
    
}
