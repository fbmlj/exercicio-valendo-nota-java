/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfacegrafica;
import agenda.Pessoa;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
/**
 *
 * @author lucio
 */
public class ListaContatos extends JPanel implements ActionListener {
    private JButton button;
    private JLabel label;
    private String[] pessoas;
    private JList lista;
    public ListaContatos(){
        
        
        pessoas = InterfaceGrafica.listatelefonica.get_lista_pessoas();
        lista = new JList (pessoas);
        lista.setFixedCellWidth(200);
        
        lista.setFixedCellHeight(30);
        JScrollPane scroll= new JScrollPane (lista);
        add(scroll);
        
        
        
        lista.addListSelectionListener(new javax.swing.event.ListSelectionListener () {

            @Override
            public void valueChanged(ListSelectionEvent e) {
               String pessoa = pessoas[lista.getSelectedIndex()];
               int i =InterfaceGrafica.listatelefonica.find_index_by_name(pessoa);
               if (i >= 0 ){
                   Pessoa pessoa_aux = InterfaceGrafica.listatelefonica.get_Pessoa(i);
                   InterfaceGrafica.formulario.set_info(pessoa_aux.get_nome(), pessoa_aux.get_telefone(), pessoa_aux.get_detalhe());
               }
               else InterfaceGrafica.formulario.remove_info();
            }
        });
    }
    
    public void atualizar_pessoas(){
        pessoas = InterfaceGrafica.listatelefonica.get_lista_pessoas();
        lista.setListData(pessoas);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e);
    }
    
}
