/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.ArrayList;

/**
 *
 * @author lucio
 */
public class Pessoa {
    private String nome;
    private String telefones;
    private String detalhe;
    public Pessoa(String nome, String telefone,String detalhe){
        this.nome = nome;
        this.telefones = telefone;
        this.detalhe = detalhe;
        
    }
    public void editar(String nome, String telefone,String detalhe){
        this.nome = nome;
        this.telefones = telefone;
        this.detalhe = detalhe;
        
    }
    
    
    public String get_nome(){
        return this.nome;
    }
    public String get_telefone(){
        return this.telefones;
    }
    public String get_detalhe(){
        return this.detalhe;
    }
   
    
}
