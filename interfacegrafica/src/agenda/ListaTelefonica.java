/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.ArrayList;

/**
 *
 * @author lucio
 */
public class ListaTelefonica {
    private  ArrayList<Pessoa> lista;
    
    public ListaTelefonica(){
        lista = new ArrayList<>();
    }
    public void adicionar_contato(String nome, String telefone, String detalhe){
        lista.add(new Pessoa(nome,telefone,detalhe));
        ordena_lista();
    }
    
    public void excluir_contato(String index){
        int i = find_index_by_name(index);
        this.lista.remove(i);
    }
    
    public void editar_contato(String cur,String nome, String telefone, String detalhe){
        int i = find_index_by_name(cur);
        lista.get(i).editar(nome, telefone, detalhe);
        ordena_lista();
    }
    
    public Pessoa get_Pessoa(int i){
        return this.lista.get(i);
    }
   
    
    public  int find_index_by_name(String nome){
        for (int i =0;i < lista.size();i++){
            if (lista.get(i).get_nome()==nome) return i;
        }
        return -1;
    }
    private void ordena_lista(){
        for(int i = 0; i < this.lista.size(); i++){
        
            for(int j = i; j < this.lista.size() - 1;j++){
                if (OrdNome.compara(lista.get(j), lista.get(j+1)) > 0){
                    troca_ordem(j,j+1);
                }
            }
        }
    }
    private void troca_ordem(int i, int j){
        Pessoa aux = lista.get(i);
        lista.set(i,lista.get(j));
        lista.set(j, aux);
    }
    public int get_quantidade_de_contatos(){
        return lista.size();
    }
    
    public String[] get_lista_pessoas(){
        int i = this.lista.size();
        String[] pessoas = new String[i+1];
        pessoas[0] ="NOVO USUARIO";
        int j;
        for (j = 0;j < i;j++)pessoas[j+1] = this.lista.get(j).get_nome();
        return pessoas;
    }
}
