package fibbonacci_sem_thread;

import java.util.Scanner;

class Fibbonacci{
  private static final Scanner input = new Scanner(System.in);
  public static void main(String[] args) {
    System.out.println("Escreva uma quantidade desejada para fibonacci:");
    calcula_fibonnaci(input.nextLong());
  }

  private static void calcula_fibonnaci(long end){
    long start = System.currentTimeMillis();
    calculo(0,1,1,end);
    System.out.println("\n\n" + (-start + System.currentTimeMillis()) + " milisegundos \n");
  }

  private static void calculo(long cont,long x,long y, long z){
    if (cont ++ >= z) return;
    System.out.println(x+"  ");
    long aux = y;
    y = x + y;
    x = aux;
    calculo(cont, x, y, z);
    
  }
}