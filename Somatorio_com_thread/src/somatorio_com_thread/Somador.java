package somatorio_com_thread;

class Somador implements Runnable {

  
  public static long limite;
  public long somador = 0;
  public long atual = 0;
  public static int passo;
  public static int quantidade = 0;

  
  public Somador(){
    quantidade++;
    passo = quantidade;
    atual = quantidade;
  }

  private void soma(){
    somador += atual;
    atual += passo;
  }

  @Override
  public void run() {
    while(atual<=limite) soma();
    
  }
}