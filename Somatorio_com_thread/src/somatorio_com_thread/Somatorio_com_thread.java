package somatorio_com_thread;

import java.util.Scanner;
import java.util.ArrayList;

class Somatorio_com_thread{
  private static ArrayList<Somador> somadores = new ArrayList<>();
  private static ArrayList<Thread> thread_list = new ArrayList<>();
  private static Scanner input = new Scanner(System.in);
  
  public static void main(String[] args) throws InterruptedException {
    
    System.out.println("O limite do somador:");
    Somador.limite = input.nextLong();
    System.out.println("Escreva o numero de thread desejado: ");
    int numero_de_thread = input.nextInt();
    
    for(int i=0;i < numero_de_thread;i++){
      somadores.add(new Somador());
      thread_list.add(new Thread(somadores.get(i)));
    }

    long start = System.currentTimeMillis();
    for(int i=0;i < numero_de_thread;i++){
      thread_list.get(i).start();
    }
    long somatorio = 0;
    for (int i =0 ; i< numero_de_thread; i++){
      thread_list.get(i).join();
      somatorio += somadores.get(i).somador;
    }

   

    System.out.println("O valor encontrado foi de:  "+ somatorio);
    
    System.out.println("O tempo necessario foi de:  "+(System.currentTimeMillis() - start));
    
  }
}